package com.cs456.helloworldmp3;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class HelloWorldmp3Activity extends Activity{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        ImageView img = (ImageView) findViewById(R.id.heartworksImage);
        final TextView txt = (TextView) findViewById(R.id.textview);
        final  MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.audioone);
        
        
        
        //Start Jon's button
        final Button jonMillsButton = (Button) findViewById(R.id.jonMillsButton);
        jonMillsButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				if(jonMillsButton.getText().equals(getText(R.string.jonMillsButtonUnclick)))
				{
					
					jonMillsButton.setText(R.string.jonMillsButtonClicked);
				}
				else{
					jonMillsButton.setText(R.string.jonMillsButtonUnclick);
				}
			}

        });

        
        
        
        //Audio player for the image
        img.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				  txt.setText(R.string.playing);
					
	          	  if(mediaPlayer.isPlaying()){
	          		  mediaPlayer.pause();
	          	  }
	          	  else{
	          		  mediaPlayer.start();
	          	  }
			}
		});   
        mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
					@Override
					public void onCompletion(MediaPlayer arg0) {
						txt.setText(R.string.poppop);
					}       	
						
        
        });
                
}
}


	
	
	
	